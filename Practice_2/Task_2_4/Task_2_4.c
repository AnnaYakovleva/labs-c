#include<stdio.h>
#include<stdlib.h>
#include<string.h>


void change(int i, int j, char str[])
{
	char tmp;
	tmp=str[j];
	str[j]=str[i];
	str[i]=tmp;
}

int main()
{
	int i,j,widthline;
	char tmp;
	char str[]="AK65L8CNU9077KK45DF4J6H34LKJ";
	printf("%s\n", str);	
	widthline=strlen(str);
	for(i=0,j=widthline-1;i<j;i++,j--)
	{
		if(str[i] >='0' && str[i] <='9')
		{
			while(!(str[j] >='A' && str[j] <= 'Z'))
			{
				j--;
			}
			change(i,j,str);
		}
		else if((str[j] >= 'A') && (str[j] <= 'Z'))
		{
			while(!(str[i] >= '0' && str[i] <= '9'))
			{
				i++;
			}
			change(i,j,str);
		}		
	}
	printf("%s\n", str);
	return 0;
}	
