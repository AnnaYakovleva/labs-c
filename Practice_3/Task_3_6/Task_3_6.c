#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define N 10 

void funcarr(int arr[N])
{
	int i;
	srand(time(0));
		for(i=0;i<N;i++)
		{
			if(rand()<RAND_MAX/2)
				arr[i]=rand()%100;
			else
				arr[i]=-(rand()%100);
		}
}


main()
{
	int arr[N];
	int i=0,sum=0;
	int elMax=0, elMin=0,pozMax=0,pozMin=0;
	funcarr(arr);	
	for(i=0;i<N;i++)
	{
		if(arr[i]>elMax)
		{
			elMax=arr[i];
			pozMax=i;
		}
		if(arr[i]<elMin)
		{
			elMin=arr[i];
			pozMin=i;
		}
	}
	if(pozMax>pozMin)
	{
		for(i=pozMin+1;i<pozMax;i++)
			sum+=arr[i];
		printf("Sum of the elements: %d\n", sum);	
	}
	else
		for(i=pozMax+1;i<pozMin;i++)
			sum+=arr[i];
		printf("Sum of the elements: %d\n", sum);			
	return 0;
}