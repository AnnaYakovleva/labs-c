#include<stdio.h>
#include<string.h>

int main()
{
	char buf[256];
	int count=0;
	int i=0,inWord=0;
	puts("Enter a string: ");
	fgets(buf,256,stdin);
	buf[strlen(buf)-1]=0;

	while(buf[i])
	{
		if(buf[i]!=' ' && inWord==0)  
		{
			count++;
			inWord=1;
		}
		else if(buf[i]==' ' && inWord==1)
			inWord=0;			
		i++;
	}
	printf("Count of words: %d\n", count);	
	return 0;
}