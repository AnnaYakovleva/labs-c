#include "Sym.h"
#include <sys/stat.h>

TSYM addCharToStruct(unsigned char buf) //���������� ������� � ���������
{
	TSYM sym;
	sym.ch = buf;
	sym.code[0] = 0;
	sym.freq = 1;
	sym.left = NULL;
	sym.right = NULL;
	return sym;
}

/*int cmp(const void* a, const void* b)
{
return ((TSYM*)a)->freq - ((TSYM*)b)->freq;
}*/

void bubbleSort(TSYM * sym, int count) //���������� �������� �� ������� ������������� �������
{
	int i, j;
	TSYM tmp;
	for (i = 0; i < count; i++)
	{
		for (j = 0; j < count - i; j++)
		{
			if ((sym + j)->freq < (sym + j + 1)->freq)
			{
				tmp = *(sym + j);
				*(sym + j) = *(sym + j + 1);
				*(sym + j + 1) = tmp;
			}
		}
	}
}


/*void printStruct(TSYM *sym)
{
	printf("%c - %f\n", sym->ch, sym->freq);
}*/

void countFrequance(TSYM sym[256], int count)//���������� ������� ������������� �������
{
	int i;
	float sum = 0;
	for (i = 0; i<count; i++)
		sum += (sym + i)->freq;
	for (i = 0; i<count; i++)
		(sym + i)->freq /= sum;
}


TSYM* buildTree(TSYM* psym[], int N)//���������� ������
{
	int i,poz=0;
	TSYM* temp = (TSYM*)malloc(sizeof(TSYM));
	temp->freq = psym[N - 2]->freq + psym[N - 1]->freq;
	temp->left = psym[N - 1];
	temp->right = psym[N - 2];
	temp->code[0] = 0;
	if (N == 2)
	return temp;
	if (N > 2)
	{
		psym[N - 1] = NULL;
		psym[N - 2] = NULL;
		for(i = 0;i < N-3;i++)
		{
			if (psym[i]->freq < temp->freq)
			{
				poz = i; //������� ��� �������
				break;
			}
		}

		for (i = N-2; i > poz; i--)
		{
			psym[i] = psym[i - 1];//���� �� ������ �������
		}
		psym[poz] = temp;//�������
	
	}
	return buildTree(psym, N - 1);
}

void makeCodes(TSYM* root)//������������ �����
{
	if (root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}

unsigned char pack(unsigned char buf[])
{
	union CODE code;
	code.byte.b1 = buf[0] - '0';
	code.byte.b2 = buf[1] - '0';
	code.byte.b3 = buf[2] - '0';
	code.byte.b4 = buf[3] - '0';
	code.byte.b5 = buf[4] - '0';
	code.byte.b6 = buf[5] - '0';
	code.byte.b7 = buf[6] - '0';
	code.byte.b8 = buf[7] - '0';
	return code.ch;
}

off_t fsize(const char *filename) //���������� ������� �����
{
	struct stat st;

	if (stat(filename, &st) == 0)
		return st.st_size;

	return -1;
}

void writeHeader(FILE *result, int count, TSYM sym[256], const char * tmp_filename, const char * original_filename)//������ ���������
{
	int i,j, tail;
	off_t originalFileSize;
	char * c;
	float * f;
	char buf[4];
	fputs("anna", result);
	fwrite(&count,sizeof(int), 1, result);
	for (i = 0; i < count; i++)
	{
		c = &((sym + i)->ch);
		fwrite(c, sizeof(char), 1, result);
		f = &((sym + i)->freq);
		fwrite(f, sizeof(float), 1, result);
	}
	tail = 8-(fsize(tmp_filename) % 8);
	fwrite(&tail, sizeof(int), 1, result);
	originalFileSize = fsize(original_filename);
	fwrite(&originalFileSize, sizeof(off_t), 1, result);
	i = 0;
	while (original_filename[i] != '.')//��������� ����������
		i++;
	for (j=0,i=i+1; j < 3; j++,i++)
	{
		buf[j] = original_filename[i];
	}
	buf[j] = '\0';
	fputs(buf, result);
}





