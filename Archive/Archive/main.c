#include"Sym.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<Windows.h>
#include <sys/stat.h>


TSYM sym[256]; //������ ��������
TSYM* psym[256]; //������ ���������� �� ���������




int main(int argc, char** argv)
{
	unsigned char buf[9],c;
	char signature[5];
	unsigned char byte;
	char OriginalFileName[20];
	char endFileName[4];
	off_t originalFileSize;
	int packOrUnpack, i,j=0,ch, tail = 0, count = 0,flag = 0, checkSize = 0,endOfFile=0;
	TSYM* root = 0;
	TSYM* rootTmp = 0;
	float * f;
	FILE *fp;
	FILE *tmpfile = NULL; 
	FILE *result;
	
	if (argc < 2)
	{
		puts("Usage: str_count filename");
		exit(1);
	}
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	printf("Enter 1, if you want to pack file. Enter 2, if you want to unpack file \n");
	scanf("%d", &packOrUnpack);
	switch (packOrUnpack)//������� ������� ���������
	{
	case 1:

		fp = fopen(argv[1], "rb");
		if (fp == 0)
		{
			perror("File error: ");
			exit(2);
		}

		while ((ch = fgetc(fp)) != -1)//���������� �������� � ���������� � ������ ��������
		{
			flag = 0;
			for (i = 0; i < count; i++)
			{
				if ((sym + i)->ch == ch)
				{
					(sym + i)->freq++;
					flag = 1;
				}
			}
			if (flag == 0)
			{
				sym[count] = addCharToStruct(ch);
				count++;
			}
		}
		countFrequance(sym, count);
		//qsort(sym, count, sizeof(TSYM), cmp);
		bubbleSort(sym, count);
		for (i = 0; i < count; i++)
			psym[i] = &sym[i];
		/*for (i = 0; i < count; i++)
			printStruct(psym[i]);*/
		root = buildTree(psym, count);
		makeCodes(root);

		rewind(fp);
		tmpfile = fopen("tmp.txt", "wb");
		if (tmpfile == 0)
		{
			perror("File error: ");
			exit(2);
		}

		while ((ch = fgetc(fp)) != -1)//���������� �������� � ������ ����� � tmpfile
		{
			for (i = 0; i < count; i++)
			{
				if ((sym + i)->ch == (unsigned char)ch)
				{
					fputs((sym + i)->code, tmpfile);
					break;
				}
			}
		}
		fclose(fp);
		fclose(tmpfile);
		tmpfile = fopen("tmp.txt", "rb");
						
		for (i = 0; argv[1][i] != '.'; i++)//������ ����� ������������� ����� � �����
		{
			OriginalFileName[i] = argv[1][i];
		}
		OriginalFileName[i] = '\0';

		result = fopen(OriginalFileName, "wb");//�������� ��������������� �����
		if (result == 0)
		{
			perror("File error: ");
			exit(2);
		}
		writeHeader(result, count, sym, "tmp.txt", argv[1]);

		i = 0;
		while (fgets(buf, 9, tmpfile))//������ tmpfile 
		{
			j = 0;
			while (buf[j])//���������� ���������� ���������
			{
				j++;
			}
			if (j < 8)
			{
				tail = 8 - j;
				for (; j < 8; j++)
				{
					buf[j] = '0';//���������� ������� ������ ������
				}
			}
			c = pack(buf);
			fwrite(&c, sizeof(unsigned char), 1, result);
			i++;
		}
		break;
	case 2:
		fp = fopen(argv[1], "rb");
		if (fp == 0)
		{
			perror("File error: ");
			exit(2);
		}
		
		fgets(signature, 5, fp); //���������� ���������
		if (strcmp(signature, "anna") != 0)
			exit(3);
		fread(&count, sizeof(int), 1, fp);
		for (i = 0; i < count; i++)
		{
			fread(&((sym + i)->ch), sizeof(char), 1, fp);
			f = &((sym + i)->freq);
			fread(f, sizeof(float), 1, fp);
		}
		fread(&tail, sizeof(int), 1, fp);
		fread(&originalFileSize, sizeof(off_t), 1, fp);
		fgets(endFileName, 4, fp);
		
		for (i = 0; i < count; i++)
			psym[i] = &sym[i];
		
		root = buildTree(psym, count);
		makeCodes(root);

		tmpfile = fopen("tmp2.txt", "wb");
		if (tmpfile == 0)
		{
			perror("File error: ");
			exit(2);
		}

		while (fread(&byte, sizeof(char), 1, fp) == 1)//���������� ������������ ������ � tmpfile
		{			
			for (i = 0; i < 8; i++)
			{
				if (byte & 1)
					fwrite("1", sizeof(char), 1, tmpfile);
				else
					fwrite("0", sizeof(char), 1, tmpfile);
				byte >>= 1;
			}
		}
		fclose(tmpfile);
		tmpfile = fopen("tmp2.txt", "rb");
		endOfFile = fsize("tmp2.txt") - tail;
		
		i = 0; //������ ����� �����
		while (argv[1][i])
		{
			OriginalFileName[i] = argv[1][i];
			i++;
		}
		OriginalFileName[i] = '\0';
		strcat(OriginalFileName, ".");
		strcat(OriginalFileName, endFileName);

		result = fopen(OriginalFileName, "wb");
		if (result == 0)
		{
			perror("File error: ");
			exit(2);
		}
		
		while (endOfFile > 0)//���������� �� ����� � ����� ����� � ������
		{
			rootTmp = root;
			do
			{
				byte = getc(tmpfile);
				endOfFile--;
				if (byte == '0')
					rootTmp = rootTmp->left;
				if (byte == '1')
					rootTmp = rootTmp->right;
			} while (rootTmp->left || rootTmp->right);

			fwrite(&(rootTmp->ch), sizeof(char), 1, result);//������ ������� � �������������� ����
		}
		fclose(tmpfile);
		fclose(result);
		
		checkSize = fsize(OriginalFileName);//�������� ������������ ��������
		if (checkSize != originalFileSize)
			exit(4);
		break;
	}
	return 0;
}