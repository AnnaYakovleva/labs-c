#ifndef _SYM_H
#define _SYM_H
#include<stdio.h>


struct SYM
{
	unsigned char ch;
	float freq;
	char code[256];
	struct SYM *left;
	struct SYM *right;
};

typedef struct SYM TSYM;

union CODE
{
	unsigned char ch;
	struct
	{
		unsigned short b1 : 1;
		unsigned short b2 : 1;
		unsigned short b3 : 1;
		unsigned short b4 : 1;
		unsigned short b5 : 1;
		unsigned short b6 : 1;
		unsigned short b7 : 1;
		unsigned short b8 : 1;
	}byte;
};


TSYM addCharToStruct(unsigned char buf);
void bubbleSort(TSYM * sym, int count);
void printStruct(TSYM *sym);
void countFrequance(TSYM sym[256], int count);
TSYM* buildTree(TSYM* psym[], int N);
void makeCodes(TSYM* root);
unsigned char pack(unsigned char buf[]);
void writeHeader(FILE *result, int count, TSYM sym[256], const char * tmp_filename);


#endif