#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

void chomp(char *buf) 
{
	//if((buf+(strlen(buf)-1))=='\n')
		buf[strlen(buf)-1]=0;
}

int getWords(char** parr, char* str)//��������� ������ ���������� �������� ������ ���� ����
{
	int i = 0, flag = 0, count = 0;
	while(str[i])
	{
		if(str[i] != ' ' && flag == 0)
		{
			flag=1;
			parr[count++]=str+i;
		}
		else if(str[i] == ' ' && flag == 1)
		{
			flag=0;
		}
		i++;
	}
	return count;
}

int wordLen(char *word, int flag) //�������, ������������ ����� �����
{
	int len=0;
	while((*word && *word!=' ') && (flag||(*word!='.' && *word!=',' && *word!='\"')))
	//���� �������� �������������� �������� ��� ����, ����� ���������� �����, �� �������� �����, ������� � �������
	//� ��������� �� ��� ������ �����
	{
		len++;
		word++;
	}
	return len;
}

void mixWord(char* parr, int len)//������������ ��������� � ������� ��� ����, ����� ����� � ����� ������������� � ��������� �������
{
	int a,b,i;
	char c;
	srand(time(0));	
	if(len<=2)
		return;
	for(i=0;i<10;i++)
	{
		a = rand()%(len-2)+1;//����� ������ � ��������� �����
		b = rand()%(len-2)+1;//����� ������ � ��������� �����
		c = *(parr + a);
		*(parr + a) = *(parr + b);
		*(parr + b) = c;
	}
}

int main()
{
	FILE* fr;
	FILE* fw;
	char buf[256];
	char* parr[256];
	int i,count;
	fr = fopen("c:\\Labs\\Practice_5\\Task_5_3\\first.txt","rt");//��������� ���� �� ������
	fw = fopen("c:\\Labs\\Practice_5\\Task_5_3\\second.txt","wt");//��������� ���� �� ������
	if(fr==0)
	{
		perror("File error: ");
		exit(1);
	}
	if(fw==0)
	{
		perror("File error: ");
		exit(1);
	}	
	while(fgets(buf,256,fr))
	{
		i=0,count=0;
		chomp(buf);
		count = getWords(parr,buf);//���������� ��������� �� �����
		for(i;i<count;i++)
		{
			mixWord(parr[i],wordLen(parr[i],0));
			fprintf(fw,"%.*s ", wordLen(parr[i],1), parr[i]);
		}			
		fprintf(fw,"%c", putchar('\n'));
	}
	fclose(fr);//��������� ���� �� �������� ������
	fclose(fw);//��������� ���� � ������� ������
	return 0;
}