#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

void printWord(char *word) //������� ��� ������ �����
{
	while(*word && *word!=' ')
		putchar(*word++);
	putchar(' ');
}

int wordLen(char *word) //�������, ������������ ����� �����
{
	int len=0;
	while(*word && *word!=' ')
	{
		len++;
		word++;
	}
	return len;
}

void chomp(char *buf) 
{
	//if((buf+(strlen(buf)-1))=='\n')
		buf[strlen(buf)-1]=0;
}

int getWords(char** parr, char* str)//��������� ������ ���������� �������� ������ ���� ����
{
	int i = 0, flag = 0, count = 0;
	while(str[i])
	{
		if(str[i] != ' ' && flag == 0)
		{
			flag=1;
			parr[count++]=str+i;
		}
		else if(str[i] == ' ' && flag == 1)
		{
			flag=0;
		}
		i++;
	}
	return count;
}

void mixStr(char** parr, int count)//������������ ��������� � ������� ��� ����, ����� ����� ������ ���������� � ��������� �������
{
	int a,b,i;
	char* c;
	srand(time(0));	
	for(i=0;i<10;i++)
	{
		a = rand()%count;
		b = rand()%count;
		c = parr[a];
		parr[a] = parr[b];
		parr[b] = c;
	}
}

int main()
{
	FILE* fr;
	FILE* fw;
	char buf[256];
	char* parr[256];
	int i,count;
	fr = fopen("c:\\Labs\\Practice_5\\Task_5_4\\first.txt","rt");//��������� ���� �� ������
	fw = fopen("c:\\Labs\\Practice_5\\Task_5_4\\second.txt","wt");//��������� ���� �� ������
	if(fr==0)
	{
		perror("File error: ");
		exit(1);
	}
	if(fw==0)
	{
		perror("File error: ");
		exit(1);
	}	
	while(fgets(buf,256,fr))
	{
		i=0,count=0;
		chomp(buf);
		count = getWords(parr,buf);//���������� ��������� �� �����
		mixStr(parr,count);	//������������ ���������
		for(i;i<count;i++) //������� ���������� ������� ���������� � ����
			fprintf(fw,"%.*s ", wordLen(parr[i]), parr[i]);
		fprintf(fw,"%c", putchar('\n'));
	}
	fclose(fr);//��������� ���� �� �������� ������
	fclose(fw);//��������� ���� � ������� ������
	return 0;
}