#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define M 20
#define N 10

void Print(char (*x)[M])//������ �������
{
	int i,j;
	for(i=0;i<N;i++)
	{
		for(j=0;j<M;j++)
			putchar(x[i][j]);
		putchar('\n');
	}
}

void ClearArr(char (*x)[M])//������� �������
{
	int i,j;
	for(i=0;i<N;i++)
		for(j=0;j<M;j++)
			x[i][j]=' ';
}

void Paint(char (*x)[M])//��������� �����
{
	int i,j,k;
	for(i=0;i<=N/2;i++)//��������� ����� ������� �������
		for(j=0;j<=M/2;j++) 
			if(rand()<RAND_MAX/2)
				x[i][j]='*';
			else
				x[i][j]=' ';
	for(i=0;i<=N/2;i++) //��������� ����� ������ �������
		for(j=M/2,k=M/2-1;j<M;j++,k--)
			x[i][j]=x[i][k];
	for(i=N/2,k=N/2-1;i<N;i++,k--)//��������� ������ ��������
		for(j=0;j<M;j++)
			x[i][j]=x[k][j];
}

int main()
{
	clock_t now;
	char str[N][M];
	srand(time(0));
	
	while(1)
	{
		ClearArr(str);//������ ������
		Paint(str);	//�������� �������
		system("cls");//������ �����
		Print(str);//����� ������� ��  �����
		now = clock();
		while(clock()<now+CLOCKS_PER_SEC);
	}
	return 0;
}
