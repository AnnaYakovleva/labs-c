#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void fillArray(int* arr,int N) //���������� �������
{		
	int i;
	srand(time(0));
	for(i=0;i<N;i++)
	{
		if(rand()<RAND_MAX/2)
			arr[i]=rand()%100;
		else
			arr[i]=-(rand()%100);
	}
}

int power(int k)//���������� ������� ������������� �������
{
	int i;
	int val = 1;
	if(k==0)
		return 1;
	else
		for(i=0;i<k;i++)
			val*=2;
	return val;
}

int rsum(int* arr, int N) //����������� ������������ ������� �� ����������
{
	if(N==1)
		return arr[0];
	else
		return rsum(arr,N/2) + rsum(arr + N/2,N-N/2);
}

int sum(int* arr, int N)//������� ������������ ��������� �������
{
	int i,summa=0;
	for(i=0;i<N;i++)
		summa+=arr[i];
	return summa;
}

int main(int argc, char** argv)
{
	int * arr;
	int N,rsumma,summa;
	double rsumTime,sumTime;
	clock_t begin, end;
	N = power(atoi(argv[1])); //��������� ���������� ��������� �������, ������� ������� ������ ����� ���������  ������
	arr = (int*)malloc(N*sizeof(int)); //������� ������������ ������
	if(arr==0) //��������� ���������� �� ������
	{
		puts("Memory error!");
		exit(1);
	}
	fillArray(arr,N);	//��������� ������
	begin = clock(); //�������� ����� ������ ���������� �������
	rsumma = rsum(arr,N); //��������� �������� � ������� ��������
	end = clock();//�������� ����� ��������� ���������� �������
	rsumTime = (double)(end-begin)/CLOCKS_PER_SEC; //���������� �����
	printf("sum array elements = %d, work time recursive summation = % .3f\n",rsumma,rsumTime);
	begin = clock();//�������� ����� ������ ���������� �������
	summa = sum(arr,N);//��������� ��������
	end = clock();//�������� ����� ��������� ���������� �������
	sumTime = (double)(end-begin)/CLOCKS_PER_SEC;//���������� �����
	printf("sum array elements = %d, work time usual summation = % .3f\n",summa, sumTime);
	free(arr);//����������� ������
	return 0;
}
