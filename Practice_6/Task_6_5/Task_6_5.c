#include<stdio.h>
#include<stdlib.h>
#include<time.h>

unsigned long fib(int n)//����������� ���������� ������������������ ���������
{
	if(n==2||n==1)
		return 1;
	else
		return fib(n-1) + fib(n-2);
}

int main(int argc, char** argv)
{
	int i,N;
	unsigned long itog;
	clock_t begin,end;
	FILE * fp;
	fp = fopen(argv[1],"wt");//��������� ���� �� ������
	if(fp==0)
	{
		perror("File error: ");
		exit(1);
	}
	puts("Enter a number: ");//����� ���������� ����� ������������������
	scanf("%d", &N);
	for(i=1;i<=N;i++)//����� � ���� � �� �����
	{
		begin = clock();
		itog = fib(i);
		end = clock();
		printf("Number: %d, Result: %d, Time: %.3f\n",i, itog,(double)(end-begin)/CLOCKS_PER_SEC);
		fprintf(fp,"Number: %d, Result: %d, Time: %.3f\n",i, itog,(double)(end-begin)/CLOCKS_PER_SEC);
	}
	return 0;
}