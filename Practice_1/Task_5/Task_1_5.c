

#include <stdio.h>
#include <string.h>

int main()
{
	char str[80];
	char format[30];
	int width;
	printf("Enter the line!\n");
	fgets(str,80,stdin);
	width = (80-strlen(str))/2+strlen(str);
	sprintf(format,"%%%ds",width);
	printf(format,str);
	return 0;
}
