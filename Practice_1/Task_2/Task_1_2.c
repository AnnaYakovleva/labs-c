
#include <stdio.h>

int main()
{
	int hour, min, sec, result;
	printf("Enter the time in the specified format: hh:mm:ss,please\n");
	result = scanf("%d:%d:%d", &hour, &min, &sec);
	if((result<3)||(hour>23||hour<0)||(min>60||min<0)||(sec>60||sec<0))
	{
		printf("You entered the wrong data!\n");
		return 1;
	}
	if((hour>=0)&&(hour<12))
	{
		printf("Good morning!\n");
	}
	if((hour>=12)&&(hour<=16))
	{
		printf("Good afternoon!\n");
	}
	if((hour>=17)&&(hour<20))
	{
		printf("Good evening!\n");
	}
	if((hour>=20)&&(hour<=23))
	{
		printf("Good night!\n");
	}
	return 0;
}
