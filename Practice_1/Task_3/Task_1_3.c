#include <stdio.h>

int main()
{
	float x,result;
	char c;
	printf("Enter the data,please\n");
	scanf("%f%c", &x, &c);
	if((c == 'D')||c == 'd')
	{
		result = x*3.14/180;
		printf("%.2fR\n", result);
	}
	else
		if((c == 'R')||c == 'r')
		{
			result = x*180/3.14;
			printf("%.2fD\n", result);
		}
		else
		{
			printf("You entered the wrong data!\n");
			return 1;
		}
	return 0;
}
